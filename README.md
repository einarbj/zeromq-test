# zeromq test

## Setup / Building

### JS

```shell
npm install
```

### C++

The code inside .cpp files is mainly copied from zeromq.org: http://zguide.zeromq.org/page:all
- The c++ binding page: http://zeromq.org/bindings:cpp

Dependency for the .cpp files:
- Fedora: `cppzmq-devel`
- Debian: `libzmq3-dev`

Building the .cpp files
- `c++ subscriber.cpp -lzmq -o subscriber`
- `c++ publisher.cpp -lzmq -o publisher`
