const zmq = require('zeromq')
  , sock = zmq.socket('pub')
  , fs = require('fs')
  , md5 = require('md5');

sock.bindSync('tcp://127.0.0.1:3000');
console.log('Publisher bound to port 3000');

const fileData = fs.readFileSync('./dummy-data');
const fileDataSize = fileData.length;
const fileDataMD5 = md5(fileData);


setInterval(() => {
  console.log('Sending data...');
  let fileDataObj = new Buffer(fileData);
  console.log(' Time: ', Date.now());
  console.log(' Data size: ', fileDataSize);
  console.log(' Data MD5: ' + fileDataMD5 + '\n');
  sock.send(['scan', 'some data']);
}, 5000);
