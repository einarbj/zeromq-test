const zmq = require('zeromq')
  , sock = zmq.socket('sub')
  , md5 = require('md5');

sock.connect('tcp://127.0.0.1:3000');
console.log('Subscriber connected to port 3000');

sock.subscribe('scan');
sock.on('message', (msg) => {
  console.log('Received data...');
  console.log(' Time: ', Date.now());
  console.log(' Topic: ', msg[0]);
  console.log(' Data size: ', msg.length);
  console.log(' Data MD5: ' + md5(msg) + '\n');
});
