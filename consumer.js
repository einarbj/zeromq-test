const zmq = require('zeromq')
  , sock = zmq.socket('pull')
  , md5 = require('md5');

sock.connect('tcp://127.0.0.1:3000');
console.log('Consumer connected to port 3000');

const receiveDataAsJSON = (msg) => {
  let dataObj = JSON.parse(msg);
  console.log(' Sender time: ', dataObj.time);
  console.log(' Transmit Time: ', (Date.now() - dataObj.time));
  console.log(' Data size: ', dataObj.data.data.length);
  console.log(' Data MD5: ' + md5(dataObj.data.data) + '\n');
}

const receiveDataRaw = (msg) => {
  console.log(' Time: ', Date.now());
  console.log(' Data size: ', msg.length);
  console.log(' Data MD5: ' + md5(msg) + '\n');
}

sock.on('message', (msg) => {
  console.log('Received data...');
  // receiveDataAsJSON(msg);
  receiveDataRaw(msg);
});
