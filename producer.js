const zmq = require('zeromq')
  , sock = zmq.socket('push')
  , fs = require('fs')
  , md5 = require('md5');

sock.bindSync('tcp://127.0.0.1:3000');
console.log('Producer bound to port 3000');

const fileData = fs.readFileSync('./dummy-data');
const fileDataSize = fileData.length;
const fileDataMD5 = md5(fileData);

const sendDataAsJSON = () => {
  let dataObj = {
    time: Date.now(),
    data: new Buffer(fileData)
  };
  const jsonObj = JSON.stringify(dataObj);

  console.log(' Time: ', dataObj.time);
  console.log(' Data size: ', fileDataSize);
  console.log(' Data MD5: ' + fileDataMD5 + '\n');
  sock.send(jsonObj);
}

const sendDataRaw = () => {
  console.log(' Time: ', Date.now());
  console.log(' Data size: ', fileDataSize);
  console.log(' Data MD5: ' + fileDataMD5 + '\n');
  sock.send(fileData);
}

setInterval(() => {
  console.log('Sending data...');
  // sendDataAsJSON();
  sendDataRaw();
}, 5000);
