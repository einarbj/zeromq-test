#include <zmq.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <unistd.h>
#include <ctime>

#if (defined (WIN32))
#include <zhelpers.hpp>
#endif

std::vector<char> data;

int readDataFromFile() {
  std::ifstream file("dummy-data",  std::ios::in|std::ios::binary|std::ios::ate);
  if (!file.is_open()) {
    std::cout << "Error opening file: dummy-data" << std::endl;
    return -1;
  }

  int dataLength = file.tellg();
  data.resize(dataLength + 5);
  data = {'s','c','a','n'};
  file.read(&data[5], dataLength);
  file.close();
  return dataLength;
}

int main () {
  zmq::context_t context (1);
  zmq::socket_t publisher (context, ZMQ_PUB);
  publisher.bind("tcp://*:5556");

  int dataLength = readDataFromFile();
  if (dataLength < 1) {
    std::cout << "Aborting..." << std::endl;
    return -1;
  }
  std::cout << "Data size: " << dataLength
    << "(" << data.size() << ")" << std::endl;

  while (1) {
    zmq::message_t message(&data[0], dataLength, NULL);
    std::cout << "Sending data - Time: " << time(0) << std::endl;
    publisher.send(message);
    sleep(5);
  }
  return 0;
}
