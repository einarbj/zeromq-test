#include <zmq.hpp>
#include <iostream>
#include <sstream>
#include <ctime>

int main (int argc, char *argv[])
{
    zmq::context_t context (1);
    zmq::socket_t subscriber (context, ZMQ_SUB);
    subscriber.connect("tcp://localhost:5556");

    subscriber.setsockopt(ZMQ_SUBSCRIBE, "scan", 4);

    while (1) {
      zmq::message_t data;
      subscriber.recv(&data);
      std::cout << "Data received - length: " << data.size() << std::endl;
      std::cout << " Time: " << time(0); << std::endl;
    }
    return 0;
}
